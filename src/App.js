import React from "react";
import "./App.css";
import Earth from "./assets/earth.svg";
import Airplane from "./assets/airplane.svg";
import { useSpring, animated } from "react-spring";
const trans1 = (x, y) => `translate3d(${x / 50}px,${y / 50}px,0)`;
const trans2 = (x, y) => `translate3d(${x / 8 + 15}px,${y / 8 - 15}px,0)`;
const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2];
function App() {
  const [props, setProps] = useSpring(() => ({
    xy: [0, 0],
    config: { mass: 10, tension: 550, friction: 140 }
  }));
  return (
    <div className="App">
      <div className="container" onMouseMove={({ clientX: x, clientY: y }) => {setProps({ xy: calc(x, y) })}}>
        <div style={{ overflow: "hidden" }}>
          What up, world? It's your boy, just one of the guys down here Well, I
          could be more specific Uh, I'm a human, and I just wanted to, you know
          For the sake of all of us earthlings out there Just wanted to say: We
          love the Earth, it is our planet We love the Earth, it is our home We
          love the Earth, it is our planet We love the Earth, it is our home
        </div>
        <animated.img
          src={Earth}
          alt="earth"
          className="Earth"
          style={{ transform: props.xy.interpolate(trans1) }}
        />
        <animated.img
          src={Airplane}
          alt="airplane"
          className="Airplane"
          style={{ transform: props.xy.interpolate(trans2) }}
        />
      </div>
    </div>
  );
}

export default App;
